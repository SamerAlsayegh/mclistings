<?php

    /**
     * Minecraft Server Status Query
     * @author Julian Spravil <julian.spr@t-online.de> https://github.com/FunnyItsElmo
     * @license Free to use but dont remove the author, license and copyright
     * @copyright © 2013 Julian Spravil
     */
    class MinecraftServerStatus {
        private $timeout;

        /**
         * Prepares the class.
         * @param int    $timeout   default(3)
         */
        public function __construct($timeout = 2) {
            $this->timeout = $timeout;
        }
			
        /**
         * Gets the status of the target server.
         * @param string    $host    domain or ip address
         * @param int    $port    default(25565)
         */
        public function getStatus($host = '127.0.0.1') {
			
			
			
			
			
            //Connect to the server
            if(!$socket = @stream_socket_client('tcp://'.$IP, $errno, $errstr, $this->timeout)) {

                //Server is offline
                return false;


            } else {

                stream_set_timeout($socket, $this->timeout);

                //Write and read data
                fwrite($socket, "\x00\x00");
				fwrite($socket, "1");
								
				
				
				
				
                fclose($socket);
                //Evaluate the received data
				
				$results = json_decode($result, TRUE);
				
				
                return $results;
            }

        }
    }

?>
