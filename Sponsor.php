<?
include 'header.php';
$type = urldecode($_GET['t']);
$server = urldecode($_GET['s']);
if ($ID != null){
echo '<div style="background: rgba(255,255,255,0.8);-webkit-border-radius: 20px; -moz-border-radius: 20px; border-radius: 20px;"><center>';	
if ($type == null){
?>

<div id="biddinginform" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
<h3 id="myModalLabel"><center>Bidding not active?</center></h3>
</div>
<div class="modal-body">
<Center>
<a class="btn btn-medium disabled">Bidding not active</a>
</Center>
<br>
 When a bid is not active, it means that it either has not started or it has already ended. 
We place a new bid every month to allow 5 servers gain sponsor spots on our list, so that they can gain more exposure.
<br>
<br>
 To check when the bid is active, you can either
check the dates listed above the bidder chart or you can subscribe to our email notifications, which are sent when a new bid is active.
If you would like to subscribe for the email notifications please click <a href="Sponsor?pl=joinbidemail">here</a>. You will have the chance to unsubscribe
from the emails whenever you want.

</div>
<div class="modal-footer">
<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
</div>
</div>


<center>
<b>Benefits of Sponsoring?</b>
<br>
<div style="width: 80%">
When you sponsor us, you don't only help us pay for the costs of running this website for free, but you also earn perks. 
Perks include front page visibility, so any players who view the website, will always view your server, before everyone else.
Another perk, is our custom made server notification system, which emails you whenever your server fails a ping 3 times.
You would then be sent an email containing the server information, including uptime, so that you can always keep your server up!
<br><br>
To sponsor us, you will need to be in the top 5 bidders at the end of the month. More details below...
<br><br>
<?

if (date('m')<= 11)
$NextMonth = date('m') + 1;
else
$NextMonth = 1;

//$start = (date('t') - 10);
//$end = (date('t') - 5);
$start = 20;
$end = 25;
$date = new DateTime(date('Y').'-'.($NextMonth).'-01');
$datemonth = $date->format('F, Y');

$date = new DateTime(date('Y').'-'.date('m').'-'.$start);
$datestart = $date->format('jS \o\f F\, Y');

$date = new DateTime(date('Y').'-'.date('m').'-'.$end);
$dateend = $date->format('jS \o\f F\, Y');
if ($_GET['bid'] == "delete"){
			if (($start <= date('jS')) && ($end >= date('jS'))){
			$PB = Query("SELECT * FROM Bidding WHERE Bidder = '$ID' ORDER BY BidAmount DESC LIMIT 1");
			if ($PB[ID] != null){
			Query("DELETE FROM Bidding WHERE Bidder = '$ID'");
			}
			else{echo "No active bids!";}
			}
			else{
			echo "You are currently unable to make changes to your bids";
			}
			}
if ($_GET['bid'] == "success"){
				if (($_POST['bid'] != null) && ($_POST['server'] != null)){
				$Server = Query("SELECT * FROM Servers WHERE ID = '".$_POST['server']."' AND Owner = '$ID' LIMIT 1");
				echo "
				<center><b>========================================================</b></center>
				<br>
				Your bid of $".$_POST['bid']." has been placed for the server with the following information:
				<br><br>
				<b>Name</b>:".$Server[Name]."<br>
				<b>IP</b>:".$Server[IP]."<br>
				<br>
				Thank you!
				<center><b>========================================================</b></center>
				
				";
				Query("INSERT INTO Bidding (BidAmount, Server, BidTime, Bidder) VALUES ('".$_POST['bid']."','".$_POST['server']."','".time()."','".$Server[Owner]."')");
				}
			}

?>
<table class="table table-condensed">
              <thead>
                <tr>
                  <th>Bidding Information</th>
                </tr>
              </thead>
              <tbody>
              	
                <tr>
                  <td>Bidding start</td>
                  <td><?echo $datestart?></td>
                </tr>
                <tr>
                  <td>Bidding end</td>
                  <td><?echo $dateend?></td>
                </tr>
                <tr>
                  <td>Bidding for the month of</td>
                  <td><?echo $datemonth?></td>
                </tr>
              </tbody>
            </table>



<table class="table table-condensed">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Server Name</th>
                  <th>Bid</th>
                  <th>Paid?</th>
                </tr>
              </thead>
              <tbody>
              	
              	<?
              	for ($a = 0; $a <= 4; $a++){
              	
				$StartMonth = strtotime(date('Y-m-'.$start));
				$EndMonth = strtotime(date('Y-m-'.$end));
				if ($a == 0)
              	$Bid = Query("SELECT * FROM Bidding WHERE BidTime >= '".$StartMonth."' AND BidTime <= '".$EndMonth."' ORDER BY BidAmount DESC LIMIT 1");
				else
              	$Bid = Query("SELECT * FROM Bidding WHERE BidTime >= '".$StartMonth."' AND BidTime <= '".$EndMonth."' ORDER BY BidAmount DESC LIMIT $a, $a");
              	$num = $a + 1;
              	if ($Bid[Bidder] != null){
              	$name = Query("SELECT * FROM Servers WHERE ID = '$Bid[Server]' LIMIT 1");
				$name = $name[Name];
				$bid = $Bid[BidAmount];
				$pay = $Bid[Paid];
				if ($pay == 1)
				$pay = "Yes";
				else
				$pay = "No";
              	?>
                <tr>
                  <td><?echo $num;?></td>
                  <td><?echo $name?></td>
                  <td>$<?echo $bid;?></td>
                  <td><?echo $pay;?></td>
                </tr>
                <?
				}
				else{
				?>
				  <tr>
                  <td><?echo $num;?></td>
                  <td>============</td>
                  <td>=====</td>
                  <td>=====</td>
                </tr>
				<?
				}
				}
                ?>
              </tbody>
            </table>
            
            
            <center><b>.: To bid for this month's sponsorship :.</b></center>
            
            <?
            $Q = Query("SELECT * FROM MailingList WHERE User = '$ID' AND EmailType = 'Bidding' LIMIT 1");
            
            if (($_GET['pl'] == "joinbidemail") && ($Q[User] == null))
			Query("INSERT INTO MailingList (User, EmailType) VALUES ('$ID','Bidding')");
			if (($_GET['pl'] == "leavebidemail")&& ($Q[User] != null))
			Query("DELETE FROM MailingList WHERE User = '$ID' AND EmailType = 'Bidding'");
			
			?>
			
			<?
			if (($server != null) || ($_POST['bid'] != null)){
			?>
			<script>
			$(function ()  
			{ $("#BidInfo").popover();  
			});
			
			function validateForm()
			{
			var x=document.forms["Bid"]["bid"].value;
			if (((x % 5) != 0) || (x <= 0) || (x == null))
			  {
			  $("#BidInfo").popover('show');
			      setTimeout(function () {
			  $("#BidInfo").popover('hide');
				    }, 4000);
			  return false;
			  }
			}
			</script>
			<br>
			<? if (($server != null) && $_GET['bid'] == "true"){?>
			<form action="Sponsor.php?bid=verify&s=<?echo $server;?>" onsubmit="return validateForm()" name="Bid" method="POST">
			<a id="BidInfo" rel="popover" 
			data-placement="left" 
			data-content="The bid must be a multiple of 5. Example: 5, 10, 15, 20, 25" 
			data-original-title="Bid Error"><input type="text" required name="bid" id="bid" placeholder="Bid (USD Dollars)"/></a>
			<input class="btn btn-inverse" value="Submit bid" type="submit"/>
			</form>
			<?
			}
			else if (($_GET['bid'] == "verify") && ($server != null) && ($_POST['bid'] != null)){
				$Servername = Query("SELECT * FROM Servers WHERE ID = '".$server."' AND Owner = '$ID' LIMIT 1");
				$Servername = $Servername[Name];
				echo "Are you sure you want to bid $".$_POST['bid'].", on your server named: <br><b>".$Servername."<b>";
				echo "<center><br>";
				?>
				
				<form action="Sponsor.php?bid=success" name="verify" method="POST">
					<input type="hidden" name="bid" id="bid" value="<?echo $_POST['bid'];?>"/>
					<input type="hidden" name="server" id="server" value="<?echo $server;?>"/>
					<input class="btn btn-success" value="Yes, bid now" type="submit"/> 
					<a class="btn btn-danger" type="button" href="index"/>No, don't bid</a>
				</form>
				
				
				<?
				echo "</center>";
			}
			}
			else{
			if ($_GET['bid'] == 'true'){
			?>
			<b><center>Please choose which server you want to bid using</center></b>
			<table class="table table-condensed">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Server Name</th>
                  <th>Option</th>
                </tr>
              </thead>
              <tbody>
              	
              	<?
              	for ($a = 0; $a <= 10; $a++){
              	if ($a == 0)
				$Query = Query("SELECT * FROM Servers WHERE Owner = '$ID' LIMIT 1");
				else
				$Query = Query("SELECT * FROM Servers WHERE Owner = '$ID' LIMIT $a, $a");
				
				$name = $Query[Name];
				
				$num = $a + 1;
              	if ($Query[ID] != null){
              	?>
                <tr>
                  <td><?echo $num;?></td>
                  <td><?echo $name?></td>
                  <td><a class="btn btn-info" href="Sponsor?bid=true&s=<?echo $Query[ID];?>">Bid</a></td>
                </tr>
                <?
				}
				}
                ?>
              </tbody>
            </table>
			
			
			<?
			}
			else if ($_GET['bid'] == "pay"){
				?>
			<table class="table table-condensed">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Server Name</th>
                  <th>Total Due</th>
                  <th>Option</th>
                </tr>
              </thead>
              <tbody>
				<?
				for ($a = 0; $a<= 4; $a++){
				if ($a == 0)
				$LIMIT = "1";
				else
				$LIMIT = "$a, $a";
				
				$Info = Query("SELECT * FROM Bidding WHERE BidTime >= '".$ThisMonth."' AND Paid = '0' AND Bidder = '$ID' ORDER BY ID DESC LIMIT ".$LIMIT);
				$Server = $Info[Server];
				$QS = Query("SELECT * FROM Servers WHERE ID = '$Server' AND Owner = '$ID' LIMIT 1");
				
				if ($QS[ID] != null){
					?>
				<tr>
                  <td><?echo $a+1;?></td>
                  <td><?echo $QS[Name]?></td>
                  <td>$<?echo $Info[BidAmount];?></td>
                  <td>
                  	<a class="btn btn-info" href="Sponsor?pay=true&s=<?echo $QS[ID];?>">Pay Now</a>
                  	
                  		<form action="https://www.paypal.com/cgi-bin/webscr" method="post" name="paypal_form">
						<p align="center">
						<input type="submit" class="btn btn-success" value="Pay now" /></p>
						<input name="cmd" type="hidden" value="_xclick" />
						<input name="business" type="hidden" value="Admin@MCListings.net" />
						<input name="item_name" type="hidden" value="Server Bid - Bid #<?echo $Info[ID];?>" />
						<input name="amount" type="hidden" value="<?echo $Info[BidAmount];?>" />
						<input name="no_shipping" type="hidden" value="1" />
						<input name="no_note" type="hidden" value="1" />
						<input name="return" type="hidden" value="http://www.mclistings.net/index?server=<?echo $Server;?>" />
						<input name="cancel_return" type="hidden" value="http://www.mclistings.net/index?server=<?echo $Server;?>" />
						<input name="rm" type="hidden" value="1" />
						<input name="currency_code" type="hidden" value="USD" /></form>
                  	
                  	
                  	
                  	</td>
                </tr>
					<?
				}
			}
			?>
			</tbody>
	       </table>
			<?
			}
			else{
	        if (($start <= date('jS')) && ($end >= date('jS'))){
			echo '<a href="Sponsor?bid=true" class="btn btn-medium">Bid now!</a>';
			$PB = Query("SELECT * FROM Bidding WHERE Bidder = '$ID' ORDER BY BidAmount LIMIT 1");
			if ($PB[ID] != null){
			echo '<a href="Sponsor?bid=delete" class="btn btn-danger-medium">Delete my bids</a>';
			}
			
			
			}
			else if (($end < date('jS')) || $start >= date('jS'))
			echo '<a href="#biddinginform" data-toggle="modal" class="btn btn-medium disabled">Bidding not active</a>';
			echo "<br><br>";
            $Q = Query("SELECT * FROM MailingList WHERE User = '$ID' AND EmailType = 'Bidding' LIMIT 1");
			if ($Q[User] != null){
			echo '<a href="Sponsor?pl=leavebidemail" class="btn btn-danger btn-small"><font size="2">Unsubscribe from mailinglist</font></a>';
			}
			else{
			echo '<a href="Sponsor?pl=joinbidemail" class="btn btn-success btn-small"><font size="2">Get informed when the next bid begins!</font></a>';
			}
}
			}
            ?>
            
            
            
</div>
<li></li>
<br>
<?













}
else if (($server != null) && ($type == 'uptime') && ($ID != null)){
$serverinfo = Query("SELECT * FROM Servers WHERE ID = '$server'");
$servername = $serverinfo[Name];
$serverip = $serverinfo[IP];
$uptimecheck = Query("SELECT * FROM Pings WHERE ServerID = '$server'");
$uptime = $uptimecheck[Uptime];

?>
<h2>Reset server uptime</h2>
<p style="width: 60%">
When you reset your server uptime, you will be resetting it to 100%. Please beware, that the difference between doing this, and resubmitting your server is
that when you resubmit your server, any small downtimes will cause your server uptime to change greatly, meanwhile using this, it will only drop it a little,
 since we have been running uptime checks for longer period.
</p>
<br><br>
Server you want to reset the uptime for:<br>
<br>
</center>
<p style="width: 400px;margin-left: auto; margin-right: auto;padding-left: 20px; padding-right: 20px ;background: rgba(255,255,255,0.8);-webkit-border-radius: 20px; -moz-border-radius: 20px; border-radius: 20px;">
Name: <?echo $servername;?><br>
IP: <?echo $serverip;?><br>
Current Uptime: <?echo $uptime;?>%<br>
</p>
<center>
<br><br>
To reset your uptime, you will need to donate $5. Once you pay it, your server uptime will automatically reset.
<br><br>


	<form action="https://www.paypal.com/cgi-bin/webscr" method="post" name="paypal_form">
		<p align="center">
		<input type="submit" class="btn btn-success" value="Purchase uptime reset" /></p>
		<input name="cmd" type="hidden" value="_xclick" />
		<input name="business" type="hidden" value="Admin@MCListings.net" />
		<input name="item_name" type="hidden" value="Server uptime reset - Server #<?echo $server;?>" />
		<input name="amount" type="hidden" value="5" />
		<input name="no_shipping" type="hidden" value="1" />
		<input name="no_note" type="hidden" value="1" />
		<input name="return" type="hidden" value="http://www.mclistings.net/index?server=<?echo $server;?>" />
		<input name="cancel_return" type="hidden" value="http://www.mclistings.net/index?server=<?echo $server;?>" />
		<input name="rm" type="hidden" value="1" />
		<input name="currency_code" type="hidden" value="USD" /></form>
<?
}
?>
</center>
</div>
<br><br>
<?
include 'footer.php';
}
else
include 'Login.php';
