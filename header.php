<?php

function sanitize_output($buffer) {

    $search = array(
        '/\>[^\S ]+/s',  // strip whitespaces after tags, except space
        '/[^\S ]+\</s',  // strip whitespaces before tags, except space
        '/(\s)+/s'       // shorten multiple whitespace sequences
    );

    $replace = array(
        '>',
        '<',
        '\\1'
    );

    $buffer = preg_replace($search, $replace, $buffer);

    return $buffer;
}

ob_start("sanitize_output");

?>
<?
include 'database.php';

?>
<body>
<div id="wrapper">
<div id="Menu">
	
<ul id="UpperMenu">
<?
if ($ID == null){
?>
<ul style="position: absolute; margin-top: 3px;margin-left: 10px;">
<form id='Login' action='index?pl=login' method='post' accept-charset='UTF-8'>
<input type='text' class="input-small" style="height: 15px;" name='Username' placeholder="Username" id='Username' required maxlength="50" /> 
<input type='password' class="input-small" style="height: 15px;" name='Password' id='Password' placeholder="Password" required maxlength="50" />
<input type='submit' style="margin-top: -8px;" class="btn btn-small btn-success" name='Submit' value='Login' />
</form>
</ul>
<?
}
?>
<ul style="position: absolute; right: 0px;">
<form class="input-append" action="/search" method="GET">
<input id="search" style="height: 15px;" placeholder="Search for a specific server" name="search" type="text" style="width:200px">
<div class="btn-group">
<button class="btn" type="submit" style="height: 26px;" tabindex="-1">Search</button>
<button class="btn dropdown-toggle" style="height: 26px;" data-toggle="dropdown" tabindex="-1">
<span class="caret"></span>
</button>
<ul class="dropdown-menu" style="">
<li><a href="/filter/Survival">Survival</a></li>
<li><a href="/filter/Creative">Creative</a></li>
<li><a href="/filter/Tekkit">Tekkit</a></li>
<li><a href="/filter/Bukkit">Bukkit</a></li>
<li><a href="/filter/FTB">Feed the beast</a></li>
<li><a href="/filter/Vanilla">Vanilla</a></li>
</ul>
</div>
</form>
</ul>
<ul id="LowerMenu">
<center>
<li><a class="NoStyle Turn" href="/index">Home</a></li>
<li><a class="NoStyle Turn" href="/NewServer">Add Server</a></li>
<li><a class="NoStyle Turn" href="/Sponsor">Sponsor</a></li>
<?
if ($ID == null){
//<br><a href="https://twitter.com/MCListingsNet" data-show-count="false" data-dnt="true"><img src="/img/twitter.png" style="width: 40px; height: auto;"/></a>

?>
<li><a class="NoStyle Turn" href="/Login">Register</a></li>
<?
}
else{
?>
<li><a class="NoStyle Turn" href="/Manage">Manage</a></li>
<li><a class="NoStyle Turn" href="#"><? echo $Username;?></a></li>
<li><a class="NoStyle Turn" href="/index.php?logout=y">Logout</a></li>
<?
}
?>
</center>
</ul>
</div>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#sponsor").click(function() {
                    
                    $.ajax({
                        type: "POST",
                        url: "cookieset.php",
                        data: "cookie=SponsoredBidding"
                    })
                });
            });

        </script>  
        <?
$page = urldecode($_GET['pl']);
$code = urldecode($_GET['i']);
$fp = urldecode($_GET['c']);
$email = $_POST['email'];
$pass = $_POST['newpass'];
$pass1 = $_POST['newpass1'];
if ($email != null){
$CheckEmail = Query("SELECT * FROM Accounts WHERE Email = '$email' LIMIT 1");
if ($CheckEmail[ID] != null){
$Username = $CheckEmail[Username];
$CODE = sha1(rand(100000000, 1000000000000000000));
Query("INSERT INTO ForgotPassword (User, Code) VALUES ('$CheckEmail[ID]','$CODE')");
$Web = 'http://www.MCListings.net/Login.php?c='.$CODE;
SendMail($email, "
<style type='text/css'>
#Title{
  box-shadow: 3px 3px 10px 10px #C2C2C2;
  background: #C2C2C2;
    -webkit-border-radius: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
}
</style>

<ul id='Title'>
<center><h2>MCListing</h2></center>
</ul>
Hello $Username, <br>
You have recently asked to have your password reset, if this is true, please read below, else ignore this email.<br>
<br>
<li>Click <a href='$Web'>this link</a>, once you do, choose your new password and you can then login!
</li>
<br><br>
If you have questions or concerns, about your registration or account security, contact us using the our <a href='http://MCListings.net/Contact'> online form</a>.<br><br>
Thank you,<br>
MCListings Administration
", "Forget Password Request for $Username");



}	
}
?>
<div id="FP" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
<h3 id="myModalLabel"><center>Forgot Password</center></h3>
</div>
<div class="modal-body">
<script type="text/javascript" >
$(function() {
$("#submitfp").click(function() {
var email = $("#email").val();
var dataString = 'email=' + email;

$.ajax({
type: "POST",
url: "Login.php?email=" + email,
data: dataString,
});
document.ForgotPass.reset();
  $("#FullEmail").hide("slow", function(){
    $(this).replaceWith("<center>An email has been dispatched to reset the password.</center>");
  });
return false;
});
});
</script>
<div id="ForgotPass" >
 <div id="FullEmail">
<form name="ForgotPass" id="ForgotPass" action="?pl=fp" method="post">
<center>
  <label for="email">Email</label>
  <div class="input-prepend">
  <span class="add-on"><i class="icon-envelope"></i></span>
  <input class="span2" style="width:150px;" name="email" id="email" type="text">
  </div>
  <br />
      <input class="btn btn-inverse" id="submitfp" type="submit" value="Send New Pass"/>
  </center>
</form>
</div>
</div>
</div>
<div class="modal-footer">
<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
</div>
</div>
<?
if ($page == "fp"){
?>
<div style="width: 70%; margin-left: auto; margin-right: auto; background: rgba(22,241,245, 1); border-radius: 10px;">
<center><strong>An email has been sent regarding your password.</strong></center>
</div>


<?
}
if (($InvalidLogin == true) && ($_POST['Username'] != null)){
?>
<div style="width: 70%; margin-left: auto; margin-right: auto; background: rgba(22,241,245, 1); border-radius: 10px;">
<center><strong>Invalid password/username<br>
<a href="#FP" data-toggle='modal' class='NoStyle'>Forgot your password?</a><br>

</strong></center>
</div>
<?
}
else if ($MustActivate == true){
?>
<div style="width: 70%; margin-left: auto; margin-right: auto; background: rgba(22,241,245, 1); border-radius: 10px;">
<center><strong>You must activate the account.<br>Check your email</strong></center>
<?
}



if ((date('jS') >= 1) && (date('jS') <= 3))
echo '<div class="alert alert-block fade in">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <center><strong>All votes have been reset for this month</strong></center>
          </div>';
if (((date('jS') >= 20) && (date('jS') <= 25)) && $_COOKIE['SponsoredBidding'] != "dismiss"){
?>
<div style="width: 70%; margin-left: auto;color: black; margin-right: auto; background: rgba(214,214,214, 0.7); box-shadow: black 1px 1px 1px;">
<center>Sponsorship bidding is currently active.</center>
</div>

<?
}
?>
<center>
<script type="text/javascript"><!--
google_ad_client = "ca-pub-5357655378256144";
/* MClistings */
google_ad_slot = "8769454514";
google_ad_width = 728;
google_ad_height = 90;
//-->
</script>
<script type="text/javascript"
src="//pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
</center>